package com.sandu.common.constant;

public class CacheConstant {

	/**
	 * 后台管理-账号
	 */
	public final static String SYS_ACCOUNT = "SYS_ACCOUNT";
	/**
	 * 后台管理-角色菜单缓存
	 */
	public final static String SYS_ROLE_MENU = "SYS_ROLE_MENU";
	
	
	/**
	 * 后台学校账号
	 */
	public final static String SCHOOL = "SCHOOL";
	/**
	 * 后台学校ACCESSTOKEN
	 */
	public final static String SCHOOL_ACCESSTOKEN = "SCHOOL_ACCESSTOKEN";
	/**
	 * 后台学校账号
	 */
	public final static String SCHOOL_ACCOUNT = "SCHOOL_ACCOUNT";
	/**
	 * 后台学校角色菜单缓存
	 */
	public final static String SCHOOL_ROLE_MENU = "SCHOOL_ROLE_MENU";
	
}
